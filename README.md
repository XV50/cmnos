**CMNos | By CMNos Dev Team**

CMNos is a OpenNos fork which was created to bring the community a free
and good project. CMNos will be updated continuely.

Current Roadmap:

• Fix current Bugs/Dupes [#buglist] 
• Finish Act 6
• Implement Act 7
• Rework TimeSpace System
• Rework Raid System
• Rework Quest System (Pointer, Goal etc.)
• Rework Act 4


If you wish to take apart on the project, make sure to join the Discord-Server.
https://discord.gg/nqmdkNt

Tutorial on how to install will be posted soon.
Roadmap will be updated soon